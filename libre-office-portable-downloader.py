#!/usr/bin/env python3
# This script downloads the latest version of LibreOffice in the folder
# where the script is located and extracts it to make a portable version.
# Ce script télécharge la dernière version de LibreOffice dans le dossier
# où le script est situé et l'extrait pour en faire une version portable.
#
# Copyright 2014 Victor Grousset <victor@tuxayo.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import sys
import urllib.request
import os

DEBUG = False

def main():
    version = get_latest_libre_office_version()
    download_deb_package_archive(version)
    clean()

def get_latest_libre_office_version():
    web_page_text = _download_web_page_containing_version_number()

    html_table_with_libre_office_versions = basic_grep(web_page_text, '<tr><td valign="top">&nbsp;</td><td><a href="')

    # last line with "<tr><td etc" contains the latest version number
    html_line_with_latest_version_number = html_table_with_libre_office_versions[-1]

    return _extract_version_number_from_line(html_line_with_latest_version_number)

def basic_grep(string, pattern):
    matches = []
    for line in iter(string.splitlines()):
        if re.match(pattern, line):
            matches.append(line)
    return matches

def _download_web_page_containing_version_number():
    url = "https://download.documentfoundation.org/libreoffice/stable/"
    response = urllib.request.urlopen(url)
    data = response.read() # a 'bytes' object
    text = data.decode('utf-8')
    return text

def _extract_version_number_from_line(line):
    version_number_pattern = '.*([0-9]\.[0-9]\.[0-9]).*'
    version_number_match = re.search(version_number_pattern, line)
    return version_number_match.group(1)

def get_link_to_libre_office_DEB_package(version_number):
    is_64bits = sys.maxsize > 2**32

    if is_64bits:
        return "https://download.documentfoundation.org/libreoffice/stable/" + version_number + "/deb/x86_64/LibreOffice_" + version_number+"_Linux_x86-64_deb.tar.gz"
    else:
        return "https://download.documentfoundation.org/libreoffice/stable/" + version_number + "/deb/x86/LibreOffice_" + version_number+"_Linux_x86_deb.tar.gz"

def download_deb_package_archive(version):
    download_link = get_link_to_libre_office_DEB_package(version)
    if not DEBUG:
        os.system("wget " + download_link + " -O libreOffice" + version + "_deb.tar.gz")

def clean():
    if not DEBUG:
        print("TODO clean")

if __name__ == '__main__':
    main()
